package com.company;

public class Box {
    private Cat cat;
    public Box(Cat cat){
        this.cat = cat;
    }
    public String open(int hours){
        if (cat.isAlive(hours) == true){
            return ("Meow");
        }
        else {
            return ("Smell");
        }
    }
}
