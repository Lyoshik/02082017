package com.company;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BoxTest {

    @Mock
    Cat cat;

    @InjectMocks
    Box box;

    @Test
    public void checkCat(){
        when(cat.isAlive(35)).thenReturn(true);
        box.open(35);
        verify(cat, times(1)).isAlive(35);
        assertEquals(box.open(35), "Meow");

    }
}
